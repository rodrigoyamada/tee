// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAzseG9UHOGMNUIq2NRSqr668Yq1OwBfNE",
    authDomain: "controle-b0a9e.firebaseapp.com",
    databaseURL: "https://controle-b0a9e.firebaseio.com",
    projectId: "controle-b0a9e",
    storageBucket: "controle-b0a9e.appspot.com",
    messagingSenderId: "698295125877",
    appId: "1:698295125877:web:d13929deca8a737b23753c",
    measurementId: "G-47W9PHZHJ8"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
