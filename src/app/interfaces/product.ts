export interface Product {
    id?: string;
    name?: string;
    picture?: string;
    createdAt?: number;
    userId?: string;
    check?: boolean;
}
